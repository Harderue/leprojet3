import csv

#le tableau avec toutes les personnes et leurs infos
TabData = {}

#func pour split et retourner un tableau, pas tres utile ici car fonction split deja faite
def SplitStr(separator, strAGuillotiner):
        strGuillotiner = strAGuillotiner.split(separator)
        #print("\nMot splitter : ",strGuillotiner)
        return strGuillotiner

def OpenAndReadCsvFile(filePath):
    #ouverture fichier
    with open(filePath, newline='') as csvfile:
        data = csv.DictReader(csvfile, delimiter='|', quotechar=' ')
        line = 0
        for row in data:
            #Temp pour temporaire :)
            Temp = {}

            Temp['adresse_titulaire'] = row['address']
            Temp['nom'] = row['name']
            Temp['prenom'] = row['firstname']
            Temp['immatriculation'] = row['immat']
            Temp['date_immatriculation'] = row['date_immat']
            Temp['vin'] = row['vin']
            Temp['marque'] = row['marque']
            Temp['denomination_commerciale'] = row['denomination']
            Temp['couleur'] = row['couleur']
            Temp['carrosserie'] = row['carrosserie']
            Temp['categorie'] = row['categorie']
            Temp['cylindree'] = row['cylindree']
            Temp['energie'] = row['energy']
            Temp['places'] = row['places']
            Temp['poids'] = row['poids']
            Temp['puissance'] = row['puissance']

            strGuillotinerdeLaFonction = SplitStr(',', row['type_variante_version'])

            Temp['type'] = strGuillotinerdeLaFonction[0]
            Temp['variante'] = strGuillotinerdeLaFonction[1]
            Temp['version'] = strGuillotinerdeLaFonction[2]

            #on ajoute la personne et ses infos dans le tab general avec ses autres potes
            TabData[line] = Temp
            #print("\n", TabData[line])
            #print("\n line++ : ", line)
            line += 1

def OpenOrCreateAndWriteCsvFile(filePath, delimiterChar):
    #Ecriture du fichier
    with open(filePath, mode='w') as auto_file:
        #delimiter -> ;
        auto_writer = csv.writer(auto_file, delimiter= delimiterChar, quotechar='"')

        #Premier ligne -> categories
            #1 ou n'importe quel ligne du tab, ils on tous els memes keyssss
        auto_writer.writerow(TabData[1].keys())

        #On ecrie les autres trucs dans l'ordre
        for x in range(0, len(TabData)):
            auto_writer.writerow(TabData[x].values())
            print("\n", TabData[x])

def MainLike(filePath):
    #main func
    OpenAndReadCsvFile(filePath)

    FileName = SplitStr('.', filePath)
    newfileName = FileName[0] + "_edited." + FileName[1]
    print("\n newfileName : ", newfileName)

    OpenOrCreateAndWriteCsvFile(newfileName, ';')

#la fonction "main" qui regroupe toutes les autres
MainLike('auto.csv')

#print("\n", TabData[0])
#print("\n", TabData[199])

"""for x in range(0, 200):
    print("\n x : ",x, " data : ", TabData[x])"""
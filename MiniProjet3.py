try:
     import csv
     import sqlite3
     import logging
     from datetime import datetime
except:
     print('smth bad with library')

#le dictionnaire avec toutes les personnes et leurs infos (ici vide, avant d ele remplir a l'ouverture du fichier csv)
TabData = {}

#Fonction pour recuperer les données du csv
def OpenAndReadCsvFile(filePath):
    #ouverture fichier
    with open(filePath, newline='') as csvfile:
        data = csv.DictReader(csvfile, delimiter=';', quotechar='"')
        line = 0
        for row in data:
            #print(line " : ", row)
            TabData[line] = row
            line += 1

#Creation de la bdd
def CreationBDD(dbName):
     try:
          conn = sqlite3.connect(dbName + '.db')
          cursor = conn.cursor()
          cursor.execute("""
          CREATE TABLE IF NOT EXISTS autoTbl(
          id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
          adresse_titulaire TEXT,
          nom TEXT,
          prenom TEXT,
          immatriculation TEXT,
          date_immatriculation DATE,
          vin INTEGER,
          marque TEXT,
          denomination_commerciale TEXT,
          couleur TEXT,
          carrosserie TEXT,
          categorie TEXT,
          cylindre INTEGER,
          energie INTEGER,
          places INTEGER,
          poids INTEGER,
          puissance INTEGER,
          type TEXT,
          variante TEXT,
          version INTEGER
          )
          """)
          conn.commit()

     except sqlite3.OperationalError:
          print('la table existe déjà')
     except Exception as e:
          lg = "Error occured : " + e + " -> return last commit"
          logging.error(lg)
          print(lg)
          conn.rollback()
     finally:
          if conn:
               conn.close()


#si id pas dans la table -> creation
def InsertionLigneBdd(dbName, id):
     try:

          conn = sqlite3.connect(dbName + '.db')
          cursor = conn.cursor()
          print(TabData[id])
          cursor.execute("""
               INSERT INTO autoTbl(adresse_titulaire, nom, prenom, immatriculation, date_immatriculation,
               vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre,
               energie, places, poids, puissance, type, variante, version) VALUES(:adresse_titulaire,
               :nom, :prenom, :immatriculation, :date_immatriculation, :vin, :marque, :denomination_commerciale,
               :couleur, :carrosserie, :categorie, :cylindree, :energie, :places, :poids, :puissance, :type, :variante,
               :version)""", TabData[id])

          conn.commit()

     except Exception as e:
          lg = "dans Insertion, Error occured : " + e
          logging.error(lg)
          print(lg)
     finally:
          if conn:
               conn.close()

#si existe deja dans la table -> on update
def UpdateLigneBdd(dbName, id):
     try:
          conn = sqlite3.connect(dbName + '.db')
          cursor = conn.cursor()
          query_ = "UPDATE autoTbl SET adresse_titulaire = ?, nom = ?, prenom = ?, immatriculation = ?, date_immatriculation = ?, \
               vin = ?, marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindre = ?, \
               energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE id = ?"

          cursor.execute(query_, (TabData[id].get("adresse_titulaire"),TabData[id].get("nom"), TabData[id].get("prenom"),\
          TabData[id].get("immatriculation"),TabData[id].get("date_immatriculation"),TabData[id].get("vin"),TabData[id].get("marque"),\
          TabData[id].get("denomination_commerciale"),TabData[id].get("couleur"),TabData[id].get("carrosserie"),TabData[id].get("categorie"),\
          TabData[id].get("cylindree"),TabData[id].get("energie"),TabData[id].get("places"),TabData[id].get("poids"), TabData[id].get("puissance"),\
          TabData[id].get("type"),TabData[id].get("variante"),TabData[id].get("version"), (id+1)))
          
          
          conn.commit()
     except Exception as e:
          lg = "dans Update, Error occured : " + e
          logging.error(lg)
          print(lg)
     finally:
          if conn:
               conn.close()

#On regarde si l'id existe dans la base
def VerifInsertion(dbName, id):
     try:
          result = False
          conn = sqlite3.connect(dbName + '.db')
          cursor = conn.cursor()

          cursor.execute("""SELECT id FROM autoTbl WHERE id = ?""", ((id+1), ))
          BddId = cursor.fetchone()
          #print(BddId)

          if BddId:
               result = True 
          else:
               result = False

     except Exception as e:
          lg = " dans Verif, Error occured : " + e
          logging.error(lg)
          print(lg)
          result = False
     finally:
          if conn:
               conn.close()
          return result


def CreaOrUpdateAllLines():
     try:
          for x in range(0, len(TabData)):
               if not VerifInsertion('Auto', x):
                    lg = "Creation Data (id = " + str(x) + ")..."
                    logging.warning(lg)
                    InsertionLigneBdd('Auto', x)
                    print("Creation... : ", x)
               else:
                    lg = "Update Data (id = " + str(x) + ")..."
                    logging.warning(lg)
                    UpdateLigneBdd('Auto', x)
                    print("Update... : ", x)
     except Exception as e:
          lg = "dans creaorupdate, Error occured : " + e
          print(lg)
          logging.error(lg)

def MainLike():
     logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
     lg = 'Program Started : ' +  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
     logging.warning(lg)

     OpenAndReadCsvFile("auto_edited.csv")
     #print(TabData)
     CreationBDD('Auto')   
     CreaOrUpdateAllLines()

     lg = 'Program ended : ' + datetime.now().strftime('%Y-%m-%d %H:%M:%S')
     logging.warning(lg)

MainLike()
